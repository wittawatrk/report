const express = require('./src/app');
const http = require('http');

const app = express();
const server = http.createServer(app);
server.listen(process.env.PORT||3000,(err) => {
    if(err){
        console.log(err)
    }
    else{
         console.log('Server Started');
    }
});
