 function getdata(req,data) {
     const conn = require('./config').connection;
     const config =require('./config').config;
     if(req.cmp==003){
       reportnumber = 123;
     }
     else reportnumber = 113;
     var sql = `SELECT 
     AMUTAK.BKSTNR as EntryNo, 
     TAS.DATUM AS Date, 
     cicmpy.crdcode AS AccountCode, 
     --AMUTAK.VOLGNR5 AS VOLGNR5, 
     cicmpy.cmp_name as AccountName, 
     (
       AMUTAK.EINDSALDO - AMUTAK.BEGINSALDO
     ) AS Amount, 
     cicmpy.bankaccountnumber AccountNo, 
     bnkacc.naam as BankName, 
     (
      CASE LTRIM(RTRIM(replace (bnkacc.naam, ' ', ''))) WHEN 'ธนาคารกรุงเทพ' THEN '002: BANGKOK BANK' WHEN 'ธนาคารกสิกรไทย' THEN '004: KASIKORNBANK' WHEN 'ธนาคารกรุงไทย' THEN '006: KRUNG THAI BANK' WHEN   'ธนาคารโตเกียว-มิตซูบิชิ ยูเอฟเจ' THEN '010: THE BANK OF TOKYO-MITSUBISHI UFJ - SAVING Acc.' WHEN   'ธนาคารทหารไทย' THEN '011: TMB BANK' WHEN   'ธนาคารไทยพาณิชย์' THEN '014: SIAM COMMERCIAL BANK' WHEN   'ธนาคารสยามซิตี้แบงค์' THEN '015: SIAM CITY BANK' WHEN   'ธนาคารซิตี้แบงค์' THEN '017: CITIBANK N.A.' WHEN   'ธนาคารซูมิโตโมมิตซุยแบงค์กิ้ง' THEN '018: SUMITOMO MITSUI BANKING' WHEN   'ธนาคารแสตนดาร์ด ชาเตอร์' THEN '020: STANDARD CHARTERED BANK (THAI)' WHEN   'ธนาคารซีไอเอ็มบีไทย' THEN '022: CIMB THAI BANK' WHEN 'ธนาคารยูโอบี' THEN '024: UNITED OVERSEAS BANK (UOB)' WHEN   'ธนาคารกรุงศรีอยุธยา' THEN '025: BANK OF AYUDHAYA' WHEN   'ธนาคารออมสิน' THEN '030: GOVERNMENT SAVING BANK' WHEN   'ธนาคารฮ่องกงและเซี่ยงไฮ้ คอร์ปอเรชั่น' THEN '031: HONGKONG & SHANGHAI CORPORATION' WHEN   'ธนาคารอาคารสงเคราะห์' THEN '033: GOVERNMENT HOUSING BANK' WHEN   'ธนาคารมิตซูโฮ คอร์ปอเรชั่น' THEN '039: MIZUHO CORPORATE BANK' WHEN   'ธนาคารธนชาติ' THEN '065: THANACHART BANK' WHEN   'ธนาคารอิสลาม' THEN '066: ISLAMIC BANK' WHEN   'ธนาคารทิสโก้' THEN '067: TISCO BANK' WHEN   'ธนาคารเกียรตินาคิน' THEN '069: KIATNAKIN BANK' WHEN   'ธนาคารไอซีบีซี' THEN '070: ICBC BANK (THAI)' WHEN   'ธนาคารไทยเครดิต' THEN '071: THAI CREDIT RETAIL BANK' WHEN   'ธนาคารแลนด์ แอนด์ เฮ้าส์' THEN '073: LAND AND HOUSES BANK' ELSE 'Could not find Bank Account' END
    ) as BankCode
   FROM 
    [${req.cmp}].[dbo].DAGBK WITH (nolock) 
     LEFT OUTER JOIN [${req.cmp}].[dbo].AMUTAK WITH (nolock) ON (AMUTAK.DAGBKNR = DAGBK.DAGBKNR) 
     LEFT OUTER JOIN (
       SELECT 
         a.dagbknr, 
         a.bkjrcode, 
         a.periode, 
         a.volgnr5, 
         a.crdnr, 
         MIN(a.datum) as Datum, 
         MIN(a.oms25) AS OMS25, 
         SUM (
           CASE WHEN (
             a.transsubtype IN ('R', 'S') 
             And a.bedrag < 0
           ) 
           OR (
             a.transsubtype NOT IN ('R', 'S') 
             And a.bedrag > 0
           ) THEN a.bedrag ELSE 0 END
         ) As Debit, 
         - SUM (
           CASE WHEN (
             a.transsubtype IN ('R', 'S') 
             And a.bedrag > 0
           ) 
           OR (
             a.transsubtype NOT IN ('R', 'S') 
             And a.bedrag < 0
           ) THEN a.bedrag ELSE 0 END
         ) As Credit, 
         (
           CASE MAX(
             CASE a.transtype WHEN 'P' THEN 1 ELSE 0 END
           ) WHEN 1 THEN 'P' ELSE Min(a.transtype) END
         ) As Transtype 
       FROM 
         [${req.cmp}].[dbo].amutas a with (nolock) 
         LEFT JOIN [${req.cmp}].[dbo].grtbk with (nolock) ON a.reknr = grtbk.reknr 
       WHERE 
         a.transtype IN ('N', 'C', 'P', 'F') 
       Group by 
         a.dagbknr, 
         a.bkjrcode, 
         a.periode, 
         a.volgnr5, 
         a.crdnr
     ) tas ON tas.dagbknr = Amutak.dagbknr 
     AND tas.bkjrcode = amutak.bkjrcode 
     AND tas.periode = amutak.periode 
     AND tas.volgnr5 = amutak.volgnr5 
     AND tas.bkjrcode IS NOT NULL 
     AND amutak.bkjrcode IS NOT NULL 
     AND tas.periode IS NOT NULL 
     AND amutak.periode IS NOT NULL 
     AND tas.volgnr5 IS NOT NULL 
     AND amutak.volgnr5 IS NOT NULL 
     LEFT JOIN [${req.cmp}].[dbo].cicmpy with (nolock) on cicmpy.crdnr = tas.crdnr 
     LEFT JOIN [${req.cmp}].[dbo].bnkacc with (nolock) on cicmpy.bankaccountnumber = bnkacc.banknr --LEFT JOIN bnkkop with (nolock) on (
     --    bnkacc.banknr = bnkacc.banknr 
     --    AND cicmpy.crdnr = bnkkop.crdnr 
     --    AND cicmpy.crdnr IS NOT NULL 
     --    AND bnkkop.crdnr IS NOT NULL ) 
   WHERE 
     AMUTAK.STATUS IN ('P', 'E', 'O') 
     AND AMUTAK.DAGBKNR IN ('${reportnumber}') 
     AND EXISTS (
       SELECT 
         amutas.id 
       FROM 
         [${req.cmp}].[dbo].amutas WITH (NOLOCK) 
       WHERE 
         amutas.bkjrcode = amutak.bkjrcode 
         AND amutas.periode = amutak.periode 
         AND amutas.dagbknr = amutak.dagbknr 
         AND amutas.volgnr5 = amutak.volgnr5 
         /* Date criteria */
         AND amutas.datum BETWEEN {d '${req.start_date}' } 
         AND {d '${req.end_date}' }
     ) 
     AND AMUTAK.entrytype NOT IN ('R') 
     AND DAGBK.DAGBKNR = '${reportnumber}' 
     AND DAGBK.TYPE_DGBK NOT IN ('W', 'T', 'R', 'V', 'I', 'M') 
     AND TAS.CRDNR != '600663' 
   ORDER BY 
     bnkacc.naam, 
     AMUTAK.BKSTNR DESC`;


     var dbcon = conn.connect(config, (err) => {
         if (err) {
             console.log(err)
         } else {
             console.log('connected')
             request = new conn.Request(dbcon);

         }
         request.query(sql, function (err, recordset) {

             if (err) console.log(err);

             data(null, recordset);
             conn.close((err) => {
                 if (err) console.log(err);
                 else console.log('connection closed');
                 return;
             })
         });

     });

 }
 module.exports = {getdata};