const express = require('express');
const router = express.Router();
const path = require('path');
module.exports = () => {
    router.use((req,res,next)=>{
        res.setHeader('Access-Control-Allow-Origin','*');
        next();
    })
    router.use('/public', express.static(path.join(__dirname, '..', '..', 'public')))
    router.post('/report', require('../controller/getData'));
  
    return router;
}